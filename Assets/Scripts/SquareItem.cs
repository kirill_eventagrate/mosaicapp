﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class SquareItem : MonoBehaviour {

    [HideInInspector]
    public VideoPlayer videoPlayer = null;
    bool isPrepere = false;
    private void Awake()
    {
        videoPlayer = gameObject.GetComponent<VideoPlayer>();
        videoPlayer.prepareCompleted += VideoPrepareCompleted;

        //videoPlayer.waitForFirstFrame = false;
        videoPlayer.isLooping = false;
        videoPlayer.loopPointReached += VideoStop;
    }
    // Use this for initialization
    void Start () {
        
        
        //videoPlayer.loopPointReached += VideoStop;
    }
    void VideoStop(VideoPlayer player)
    {
        if (!GameManagerScript.instance.isPlayVideo)
        {
            GameManagerScript.instance.PlayRandomVideo();
        }
    }
    void VideoPrepareCompleted(VideoPlayer source)
    {
        if (!isPrepere)
        {
            RenderTexture rt;
            rt = new RenderTexture(120, 120, 0, RenderTextureFormat.ARGB32);
            rt.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
            rt.Create();

            gameObject.GetComponent<RawImage>().texture = rt;
            gameObject.GetComponent<RawImage>().color = new Color32(255, 255, 255, 255);
            videoPlayer.targetTexture = rt;

            isPrepere = true;
            Loom.QueueOnMainThread(() => {
                PlayVideoTest();
            }, Random.Range(.3f, 2f));
        }
    }
    public void PrepareVideo(string url)
    {
        isPrepere = false;
        if (videoPlayer.isPlaying)
            videoPlayer.Stop();
        videoPlayer.url = url;
        videoPlayer.Prepare();
    }
    public void StartVideo()
    {
        Loom.QueueOnMainThread(() => {
            videoPlayer.Play();
        }, Random.Range(.3f, 1.5f));
    }
    public void StopVideo()
    {
        videoPlayer.Stop();
    }

    void PlayVideoTest()
    {
        videoPlayer.Play();
        Invoke("StopVideo", .9f);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
