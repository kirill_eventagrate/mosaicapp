﻿#if UNITY_WEBGL && !UNITY_EDITOR
#else
#define MULTITHREADING_ENABLED
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
#endif
using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// delegate to decode necessary actions from object from the worker yield return
    /// </summary>
    /// <param name="o">this is an object that was returned from the DoBackgroundWork via yield return</param>
    /// <returns>return 0 if you don't want any actions, return positive number to await for Continue command for
    /// the specified amount of time, return float.PositiveInfinity to await for Continue, return negative number
    /// to just await for the specified time without a possibility to interrupt this awaitening</returns>
    public delegate float InterruptionRule(object o);

    public class UnityBackgroundWorker : MonoBehaviour
    {
        public static int MainThreadId = int.MinValue;

        private static UnityBackgroundWorker _instance;

        [Range(0.01f, 1f)]
        public float UpdateTime = 0.2f;
        [Range(10, 300)]
        public int EventUpdateTime = 150;
        public ContinuationMode Mode = ContinuationMode.AwaitMessages;

        public enum ContinuationMode
        {
            StrictAwait,    // worker pauses execution until Continue()
            AwaitMessages,  // worker executes until the first not null message from DoBackgroundWork
            AwaitPauses,    // worker executes until the first message with another await command
            DontAwait
        }

        private Coroutine _coroutine;
        private int _continue;
#if MULTITHREADING_ENABLED
        private BackgroundWorker _bw;
        private Queue<Action> _actionsQueue;
#else
        private bool _cancelled;
#endif

        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this);
                return;
            }
            _instance = this;
#if MULTITHREADING_ENABLED
            MainThreadId = Thread.CurrentThread.ManagedThreadId;
            _actionsQueue = new Queue<Action>(64);
            _bw = new BackgroundWorker();
            _bw.WorkerSupportsCancellation = true;
            _bw.DoWork += DoBackgoroundWork;
            _bw.RunWorkerCompleted += OnWorkCompleted;
            Debug.Log("UnityBackgroundWorker started in PARALLEL mode");
#else
            Logger.Say("UnityBackgroundWorker started in COROUTINE mode");
#endif
        }

#if MULTITHREADING_ENABLED
        void OnEnable()
        {
            _coroutine = StartCoroutine(Ever());
        }

        // ReSharper disable CompareOfFloatsByEqualityOperator
        private void DoBackgoroundWork(object sender, DoWorkEventArgs e)
        {
            Request r = (Request)e.Argument;
            IEnumerator en = r.ActionP != null ? r.ActionP(r.Argument) : r.Action();
            e.Result = r.EndCallback;
            if (_bw.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            while (en.MoveNext())
            {
                if (_bw.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                if (en.Current == null)  // if it's just a null for synchronious compatibility
                    continue;
                // if it's a message - wait for Continue()
                if (Mode == ContinuationMode.AwaitMessages)
                    AwaitForContinue();
                float delay = 0f;
                if (r.InterruptionRule != null) // if rule to decode the delay time provided we can estimate the await timeout
                    delay = r.InterruptionRule(en.Current);
                // if it's an await message - wait for Continue() and then do ProgressCallback
                if (delay != 0f && Mode == ContinuationMode.AwaitPauses)
                    AwaitForContinue();
                if (r.ProgressCallback != null)
                {
                    object o = en.Current;
                    Invoke(() => r.ProgressCallback(o));
                }
                if (delay == 0f) continue;
                if (delay > 0f) // await with interruption and possibility to continue
                {
                    _awaitEnds = float.IsPositiveInfinity(delay) ? DateTime.MaxValue :
                        DateTime.Now + TimeSpan.FromSeconds(delay);
                    lock (this)
                    {
                        _continue--;
                    }
                    if (Mode == ContinuationMode.StrictAwait)
                        AwaitForContinue();
                }
                else // await without interruption (not possible to continue earlier)
                    Thread.Sleep((int)(-1000f * delay));
            }
        }
        // ReSharper restore CompareOfFloatsByEqualityOperator

        private DateTime _awaitEnds;
        private void AwaitForContinue()
        {
            if (_continue >= 0) return;
            while (DateTime.Now < _awaitEnds)
            {
                Thread.Sleep(EventUpdateTime);
                lock (this)
                {
                    if (_continue < 0) continue;
                }
                break;
            }
        }

        public static void Continue()
        {
            if (_instance == null) return;
            lock (_instance)
            {
                _instance._continue++;
            }
        }

        private void OnWorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _continue = 0;
            if (e.Cancelled) return;
            Action action = (Action)e.Result;
            Invoke(() => action());
        }

        private void Invoke(Action callback)
        {
            lock (this)
            {
                _actionsQueue.Enqueue(callback);
            }
        }

        private IEnumerator Ever()
        {
            while (true)
            {
                lock (this)
                {
                    while (true)
                    {
                        if (_actionsQueue.Count < 1) break;
                        _actionsQueue.Dequeue()();
                    }
                }
                yield return new WaitForSeconds(UpdateTime);
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private class Request
        {
            public Request(Func<object, IEnumerator> actionp,
                object argument, Action endCallback, Action<object> progressCallback, InterruptionRule interruptionRule)
            {
                ActionP = actionp;
                Argument = argument;
                EndCallback = endCallback;
                ProgressCallback = progressCallback;
                InterruptionRule = interruptionRule;
            }
            public Request(Func<IEnumerator> action,
                Action endCallback, Action<object> progressCallback, InterruptionRule interruptionRule)
            {
                Action = action;
                EndCallback = endCallback;
                ProgressCallback = progressCallback;
                InterruptionRule = interruptionRule;
            }
            public readonly Func<object, IEnumerator> ActionP;
            public readonly Func<IEnumerator> Action;
            public readonly object Argument;
            public readonly Action EndCallback;
            public readonly Action<object> ProgressCallback;
            public readonly InterruptionRule InterruptionRule;
        }
#else
        private float _awaitTill;
        // ReSharper disable CompareOfFloatsByEqualityOperator
        private IEnumerator DoBackgoroundWork(IEnumerator en,
            Action endCallback, Action<object> progressCallback, InterruptionRule interruptionRule)
        {
            yield return null; // all calculations will start in the next frame
            while (en.MoveNext())
            {
                if (_cancelled)
                    break;
                if (en.Current == null)
                {
                    yield return null;
                    continue;
                }
                if (Mode == ContinuationMode.AwaitMessages
                    && _continue < 0)
                    while (Time.time < _awaitTill)
                    {
                        yield return new WaitForSeconds(EventUpdateTime * 0.001f);
                        if (_continue < 0) continue;
                        break;
                    }
                float delay = 0f;
                if (interruptionRule != null)
                    delay = interruptionRule(en.Current);
                if (delay != 0f && Mode == ContinuationMode.AwaitPauses
                    && _continue < 0)
                    while (Time.time < _awaitTill)
                    {
                        yield return new WaitForSeconds(EventUpdateTime * 0.001f);
                        if (_continue < 0) continue;
                        break;
                    }
                if (progressCallback != null)
                    progressCallback(en.Current);
                if (delay == 0f)
                {
                    yield return null;
                    continue;
                }
                if (delay > 0f) // await with interruption and possibility to continue
                {
                    float spent = 0f;
                    _continue--;
                    if (Mode == ContinuationMode.StrictAwait)
                        while (spent < delay)
                        {
                            yield return new WaitForSeconds(EventUpdateTime * 0.001f);
                            spent += EventUpdateTime * 0.001f;
                            if (_continue < 0) continue;
                            break;
                        }
                    else _awaitTill = Time.time + delay;
                }
                else // await without interruption (not possible to continue earlier)
                    yield return new WaitForSeconds(-delay);
            }
            if (!_cancelled)
                endCallback();
            _coroutine = null;
            _continue = 0;
        }
        // ReSharper restore CompareOfFloatsByEqualityOperator

        public static void Continue()
        {
            if (_instance == null) return;
            _instance._continue++;
        }
#endif

        void OnDisable()
        {
#if MULTITHREADING_ENABLED
            if (_bw.IsBusy)
            {
                Debug.Log("Background async process was Cancelled");
                _bw.CancelAsync();
            }
#endif
            if (_coroutine != null)
                StopCoroutine(_coroutine);
        }

        private void LocalRun(Func<object, IEnumerator> action,
            object argument, Action endCallback, Action<object> progressCallback, InterruptionRule interruptionRule)
        {
#if MULTITHREADING_ENABLED
            if (!_bw.IsBusy)
                _bw.RunWorkerAsync(new Request(action, argument, endCallback, progressCallback, interruptionRule));
#else
            _cancelled = false;
            if (_coroutine != null)
                Logger.Say(2, "Duplicated envocation of BackgroundWorker.Run");
            _coroutine = StartCoroutine(DoBackgoroundWork(action(argument), endCallback, progressCallback, interruptionRule));
#endif
        }
        private void LocalRun(Func<IEnumerator> action,
            Action endCallback, Action<object> progressCallback, InterruptionRule interruptionRule)
        {
#if MULTITHREADING_ENABLED
            if (!_bw.IsBusy)
                _bw.RunWorkerAsync(new Request(action, endCallback, progressCallback, interruptionRule));
#else
            _cancelled = false;
            if (_coroutine != null)
                Logger.Say(2, "Duplicated envocation of BackgroundWorker.Run");
            _coroutine = StartCoroutine(DoBackgoroundWork(action(), endCallback, progressCallback, interruptionRule));
#endif
        }

        public static void Run(Func<object, IEnumerator> action,
            object argument, Action endCallback, Action<object> progressCallback = null, InterruptionRule interruptionRule = null)
        {
            if (_instance == null || !_instance.isActiveAndEnabled)
            {
                Debug.Log("UnityBackgroundWorker is not instantiated or enabled");
                return;
            }
            _instance.LocalRun(action, argument, endCallback, progressCallback, interruptionRule);
        }
        public static void Run(Func<IEnumerator> action,
            Action endCallback, Action<object> progressCallback = null, InterruptionRule interruptable = null)
        {
            if (_instance == null || !_instance.isActiveAndEnabled)
            {
                Debug.Log("UnityBackgroundWorker is not instantiated or enabled");
                return;
            }
            _instance.LocalRun(action, endCallback, progressCallback, interruptable);
        }

        public static void Stop()
        {
            if (_instance == null || !_instance.isActiveAndEnabled)
            {
                Debug.Log("UnityBackgroundWorker is not instantiated or enabled");
                return;
            }
#if MULTITHREADING_ENABLED
            if (_instance._bw.IsBusy)
                _instance._bw.CancelAsync();
#else
            _instance._cancelled = true;
#endif
        }
    }
}