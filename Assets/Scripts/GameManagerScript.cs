﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class GameManagerScript : MonoBehaviour {

    public static GameManagerScript instance;

    public string path = "E:/Builds/";
    //public GameObject previewContainer;
    public GameObject preview;
    public GameObject mainPanel;
    
    public GameObject settingsPanel;
    public InputField settingInput;

    public GameObject logoPanel;
    public GameObject logoPanel2;
    public GameObject thirdPanel;

    VideoPlayer videoPlayer;

    GameObject[] previewItems;
    List<FileInFolder> fileList = new List<FileInFolder>();
    Queue<FileInFolder> showFilesQueu = new Queue<FileInFolder>();
    List<int> freePosition = new List<int>();
    Stack<string> newFilesQueu = new Stack<string>();
    Color[] colors = new Color[3];
    [HideInInspector]
    public bool isPlayVideo = false;
    int indexOfColoredSquares = 0;
    int maxPreviewIsPlay = 0;

    private void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start () {

        if (PlayerPrefs.HasKey("Path"))
            path = PlayerPrefs.GetString("Path");

        settingInput.text = path;
        settingInput.onEndEdit.AddListener(delegate(string str) {
            PlayerPrefs.SetString("Path", str);
            path = PlayerPrefs.GetString("Path");
            Debug.Log(path);
        });
        previewItems = GameObject.FindGameObjectsWithTag("preview");
        
        for (int i = 0; i < 144; i++) {
            freePosition.Add(i);
        }


        videoPlayer = mainPanel.GetComponent<VideoPlayer>();
        videoPlayer.prepareCompleted += VideoPrepareCompleted;
        videoPlayer.loopPointReached += VideoStop;
        if (Directory.Exists(path)) {
            InvokeRepeating("InitColor", 1f, .15f);
        }else{
            settingsPanel.SetActive(true);
        }
    }


    void InitColor()
    {
        if (freePosition.Count > 0) {
            colors[0] = new Color32(255, 130, 1, 255);
            colors[1] = new Color32(2, 173, 110, 255);
            colors[2] = new Color32(0, 127, 196, 255);
            int k = Random.Range(0, freePosition.Count);
            int j = freePosition[k];
            int i = Random.Range(0, 3);
            
            previewItems[j].GetComponent<RawImage>().color = colors[i];
            freePosition.RemoveAt(k);
        }else{
            for (int i = 0; i < 144; i++)
            {
                freePosition.Add(i);
            }
            CancelInvoke("InitColor");
            StartCoroutine(FadeImage(logoPanel2.GetComponent<Image>(), true));
            StartCoroutine(FadeImage(thirdPanel.GetComponent<Image>(), false));
            if (!CheckCountFiles(30))
            {
                Invoke("ClearSquares", 5f);
            }
            else
            {
                Invoke("StartParseVideo", 2.5f);
            }
        }
        if(freePosition.Count == 90)
        {
            StartCoroutine(FadeImage(logoPanel.GetComponent<Image>(), true));
            StartCoroutine(FadeImage(logoPanel2.GetComponent<Image>(), false));
        } 
    }

    void ClearSquares()
    {
        foreach (GameObject ob in previewItems)
        {
            ob.GetComponent<RawImage>().texture = null;
            ob.GetComponent<RawImage>().color = Color.white;
        }
        StartCoroutine(FadeImage(thirdPanel.GetComponent<Image>(), true));
        StartCoroutine(FadeImage(logoPanel.GetComponent<Image>(), false));
        //CancelInvoke("CheckNewFiles");
        InvokeRepeating("InitColor", 1f, .15f);
    }

    void StartParseVideo()
    {
        StartCoroutine(FadeImage(thirdPanel.GetComponent<Image>(), true));
        InvokeRepeating("CheckNewFiles", .3f, 5f);
    }
    
    void ShowNewItems()
    {
        if (showFilesQueu.Count > 0)
        {
            //Debug.Log("indexOfColoredSquares is " + indexOfColoredSquares);
            //Debug.Log("showFilesQueu count is " + showFilesQueu.Count);
            indexOfColoredSquares++;
            FileInFolder fif = showFilesQueu.Dequeue();
            previewItems[fif.GetPosition()].GetComponent<SquareItem>().PrepareVideo(path + fif.GetFileName());
        }
        else
        {
            CancelInvoke("ShowNewItems");
            indexOfColoredSquares = 0;
            Loom.QueueOnMainThread(() => {
                PlayNewFiles();
            }, 3f);
        }
    }

    void UpdateFileList()
    {
        int k = 0;
        int j = 0;
        foreach (string fileName in newFilesQueu)
        {

            if (freePosition.Count > 0)
            {
                //Debug.Log("freePosition is " + freePosition.Count);
                k = Random.Range(0, freePosition.Count - 1);
                j = freePosition[k];
                freePosition.RemoveAt(k);
                fileList.Add(new FileInFolder(j, fileName));
                if(showFilesQueu.Count < 144)
                    showFilesQueu.Enqueue(new FileInFolder(j, fileName));
            }
            if (freePosition.Count == 0)
            {
                //Debug.Log("freePosition is 0");
                for (int i = 0; i < 143; i++)
                {
                    freePosition.Add(i);
                }
            }
        }
        
        Debug.Log("fileList is " + fileList.Count);
        Debug.Log("newFilesQueu is " + newFilesQueu.Count);
        Debug.Log("showFilesQueu is " + showFilesQueu.Count);
        
        
        //newFilesQueu.Clear();
        Loom.QueueOnMainThread(() => {
            indexOfColoredSquares = 0;
            InvokeRepeating("ShowNewItems", .2f, .15f);
        }, .3f);
    }

    void VideoPrepareCompleted(VideoPlayer source)
    {
        iTween.ScaleTo(mainPanel, Vector3.one, 2f);
        videoPlayer.Play();
    }

    void VideoStop(VideoPlayer player)
    {
        iTween.ScaleTo(mainPanel, Vector3.zero, 2f);
        /*if (newFilesQueu.Count > 0)
            Loom.QueueOnMainThread(() => {
                PlayNewFiles();
            }, 2f);
        else
        {
            
        }*/
        isPlayVideo = false;
        maxPreviewIsPlay = fileList.Count <= 35 ? fileList.Count : 35;
        Debug.Log("maxPreviewIsPlay " + maxPreviewIsPlay);
        InvokeRepeating("PlayPreviewVideo", .5f, .15f);
    }

    void PlayNewFiles()
    {
        string fileName = newFilesQueu.Pop();
        videoPlayer.url = path + fileName;
        videoPlayer.Prepare();
        newFilesQueu.Clear();
    }



    void CheckNewFiles()
    {
        if (!isPlayVideo)
        {
            DirectoryInfo dir = new DirectoryInfo(@path);
            if (fileList.Count < dir.GetFiles().Length)
            {
                isPlayVideo = true;
                StopPreviewVideo();
                Debug.Log("New file!\nLast filecount: " + fileList.Count + "\nnow: " + dir.GetFiles().Length);
                foreach (var item in dir.GetFiles())
                {
                    if (!CheckFileName(item.Name))
                    {
                        newFilesQueu.Push(item.Name);
                    }
                }
                UpdateFileList();
            }
        }
    }

    bool CheckCountFiles(int count)
    {
        DirectoryInfo dir = new DirectoryInfo(@path);
        if (dir.GetFiles().Length >= count){
            return true;
        }else{
            return false;
        }
    }

    void StopPreviewVideo()
    {
        
        foreach (GameObject ob in previewItems)
        {
            if (ob.GetComponent<SquareItem>().videoPlayer.isPlaying)
            {
                ob.GetComponent<SquareItem>().videoPlayer.Stop();
            }
        }
    }

    void PlayPreviewVideo()
    {
        //Debug.Log("indexOfColoredSquares " + indexOfColoredSquares);
        
        if(indexOfColoredSquares <= maxPreviewIsPlay)
        {
            previewItems[fileList[indexOfColoredSquares].GetPosition()].GetComponent<SquareItem>().StartVideo();
            indexOfColoredSquares++;
        }
        else
        {
            CancelInvoke("PlayPreviewVideo");
            indexOfColoredSquares = 0;
        }
    }

    public void PlayRandomVideo()
    {
        int index = Random.Range(0, fileList.Count);
        if (previewItems[fileList[index].GetPosition()].GetComponent<SquareItem>().videoPlayer.isPlaying)
        {
            PlayRandomVideo();
        }
        else
        {
            previewItems[fileList[index].GetPosition()].GetComponent<SquareItem>().videoPlayer.Play();
        }
    }

    IEnumerator FadeImage(Image img, bool fadeAway)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= -1; i -= Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }

    bool CheckFileName(string name)
    {
        foreach (var item in fileList)
        {
            if (name == item.GetFileName())
                return true;
        }
        return false;
    }

    

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.LeftAlt)){
            settingsPanel.SetActive(!settingsPanel.activeSelf);
        }
        if (Input.GetKeyDown(KeyCode.RightAlt)) {
            if (Directory.Exists(path))
            {
                SceneManager.LoadScene("main");
            }
            else
                Debug.Log("path don't find");
        }
    }
}

class FileInFolder
{
    int position;
    string pathBig;
    string pathSmall;
    public FileInFolder(int _position, string _pathBig)
    {
        position = _position;
        pathBig = _pathBig;
    }
    public string GetFileName() { return pathBig; }
    public int GetPosition() { return position; }
}