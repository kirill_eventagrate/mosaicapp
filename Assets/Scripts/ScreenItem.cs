﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ScreenItem : MonoBehaviour {


    int animIndex = 0;
    float animSpeed = 5f;
    float animSpeedRepeat = 3.5f;
    float move = 1;
    VideoPlayer videoPlayer;
    
    // Use this for initialization
    void Start () {

        videoPlayer = gameObject.GetComponent<VideoPlayer>();
        videoPlayer.Prepare();
        videoPlayer.prepareCompleted += VideoPrepareCompleted;
        // x: (-5.46;4.3)
        // y: (-3.47;2.18)
        // z: (-1.5;3.5)
        Vector3 randomPosition = new Vector3(Random.Range(-4.5f, 4f), Random.Range(-1f, 3.5f), Random.Range(-0.5f, 3.5f));
        animIndex = Random.Range(0, 3);
        transform.position = randomPosition;
        //transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 90);
        /*switch (animIndex)
        {
            case 0:
                InvokeRepeating("MoveFrontBack", Random.Range(0f, 1f), animSpeedRepeat); break;
            case 1:
                InvokeRepeating("MoveLeftRight", Random.Range(0f, 1f), animSpeedRepeat); break;
            case 2:
                InvokeRepeating("RotationItem", Random.Range(0f, 1f), animSpeedRepeat); break;
        }*/
        InvokeRepeating("MoveFrontBack", Random.Range(0f, 1f), animSpeedRepeat);
    }

    void VideoPrepareCompleted(VideoPlayer source)
    {
        source.Play();
        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }
    void MoveFrontBack()
    {
        Vector3 position = new Vector3(transform.position.x, transform.position.y, transform.position.z + (0.8f * move));
        //float time = Random.Range(0, animSpeed);
        float time = animSpeed;
        iTween.MoveTo(gameObject, position, time);
        move *= -1;
    }



    // если понадобится
    void MoveLeftRight()
    {
        Vector3 position = new Vector3(transform.position.x + (0.8f * move), transform.position.y, transform.position.z);
        float time = Random.Range(0, animSpeed);
        iTween.MoveTo(gameObject, position, time);
        move *= -1;
    }

    void RotationItem()
    {
        Vector3 angel = new Vector3(transform.position.x, transform.position.y, transform.position.z + (15 * move));
        float time = Random.Range(0, animSpeed);
        iTween.RotateTo(gameObject, angel, time);
        move *= -1;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
