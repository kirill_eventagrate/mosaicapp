﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

public class NumericalTouchControlScript : MonoBehaviour/*, IPointerEnterHandler, IPointerExitHandler*/ {


	static bool isDraggingSome = false;


	public Action<float> OnChange = (_) => {};

	public float holdActivateTime = 1f;
	public Color activeColor = Color.yellow;
	public Color normalColor = Color.white;
	public Color errorColor = Color.red;
    
	InputField inputField;

	RectTransform rectTransform;

	Canvas parentCanvas;
	CanvasGroup parentCanvasGroup;

	public bool pointerOver = false;


	float timeStartedHolding = -1;
	public bool isDragging = false;

	public float offsetYScale = 0.3f;

	float currentValue = -1;
	float prevValue = -1;

	Vector2 startDraggingPos;

	Vector3 prevDraggingPos;


	// Use this for initialization
	void Awake () {
		rectTransform = GetComponent<RectTransform> ();
		parentCanvas = GetComponentInParent<Canvas> ();
		parentCanvasGroup = GetComponentInParent<CanvasGroup> ();

		inputField = GetComponentInChildren<InputField> ();
		inputField.shouldHideMobileInput = true;
		inputField.onValueChanged.AddListener (OnInputValueChange);

		currentValue = ParseCurrentValue ();
	}
	
	// Update is called once per frame
	void Update () {
		var m_p = Input.mousePosition;
		if (Input.touchCount > 0) {
			m_p = new Vector3(Input.touches [0].position.x, Input.touches [0].position.y, 0);
		}
		m_p.y = Screen.height - m_p.y;
		pointerOver = RectTransformToScreenSpace (rectTransform).Contains (m_p);
        
		if (Input.GetMouseButton(0) && (parentCanvasGroup == null || parentCanvasGroup.interactable)) {
            
			if (pointerOver) {
				// check if start dragging

				//Debug.Log ("check if start dragging");

				if (timeStartedHolding == -1) {
					timeStartedHolding = Time.time;
				}

				if (Time.time - timeStartedHolding > holdActivateTime) {
					// activate dragging

					if (!isDragging && !isDraggingSome) {
						StartDragging ();
					}

				}

			} else {
				/*timeStartedHolding = -1;
				if (isDragging) {
					StopDragging ();
				}*/
			}
		} else {
			timeStartedHolding = -1;
			if (isDragging) {
				StopDragging ();
			}
		}




		if (isDragging) {
			var mousePos = Input.mousePosition;
			var screenspaceRect = RectTransformToScreenSpace (rectTransform);

			var deltaX = (mousePos.x - prevDraggingPos.x) / screenspaceRect.width;
			var deltaY = Mathf.Abs (mousePos.y - startDraggingPos.y) /  screenspaceRect.width;



			//Debug.Log ("sf: "+parentCanvas.scaleFactor);
			//Debug.Log ("deltaY: "+deltaY);
			//Debug.Log ("deltaX: "+deltaX);

			var valSpeed = Mathf.Pow(deltaY,3) * offsetYScale * 10000;

			var newVal = prevValue + deltaX * valSpeed;
			SetCurrentValue (newVal);

			prevDraggingPos = mousePos;
			prevValue = currentValue;
		}
	}


	// METHODS

	public void SetErrorColor(){
		inputField.GetComponent<Image> ().color = errorColor;
	}

	public void SetNormalColor(){
		inputField.GetComponent<Image> ().color = normalColor;
	}

	public void SetActiveColor(){
		inputField.GetComponent<Image> ().color = activeColor;
	}

	float ParseCurrentValue(){
		try{
			return float.Parse (inputField.text);
		}catch{
		}

		return -1;
	}

	public void SetCurrentValue(float newVal){
		if (newVal == currentValue)
			return;

		//Debug.Log ("SetCurrentValue "+newVal);

		currentValue = (float)Math.Round( newVal, 3);
		OnChange (newVal);

		inputField.text = currentValue.ToString();
	}

	public float CurrentValue {
		get { 
			return currentValue;
		}
	}

	void StartDragging(){
		isDragging = true;
		var mousePos = Input.mousePosition;
		startDraggingPos.x = mousePos.x;
		startDraggingPos.y = mousePos.y;
		prevValue = currentValue;

		SetActiveColor ();

		isDraggingSome = true;
	}

	void StopDragging(){
		isDragging = false;
		SetNormalColor ();
		isDraggingSome = false;

	}


	public static Rect RectTransformToScreenSpace(RectTransform transform)
	{
		Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
		Rect rect = new Rect(transform.position.x, Screen.height - transform.position.y, size.x, size.y);
		rect.x -= (transform.pivot.x * size.x);
		rect.y -= ((1.0f - transform.pivot.y) * size.y);
		return rect;
	}


	// EVENTS



	void OnInputValueChange(string val){
		//Debug.Log ("OnInputValueChange");
		try{
			SetCurrentValue (float.Parse (inputField.text));
		}catch{
		}

	}


//	public void OnPointerEnter(PointerEventData eventData){
//		Debug.Log ("OnPointerEnter");
//		pointerOver = true;
//	}
//
//	public void OnPointerExit(PointerEventData eventData){
//		Debug.Log ("OnPointerExit");
//		pointerOver = false;
//	}



}
