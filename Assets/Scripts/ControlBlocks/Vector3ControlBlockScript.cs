﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Vector3ControlBlockScript : MonoBehaviour {

	public Action<Vector3> OnChange = (_) => {};

	public bool autoInit = true;

	public ScalarControlBlockScript xScalarCB, yScalarCB, zScalarCB;

	public string title, playerPrefsKey, oscAddress;
	public Text titleLabel;

	public Vector3 value;

	public Vector3 defaultValue = Vector3.zero;


	void Awake(){
		xScalarCB.autoInit = yScalarCB.autoInit = zScalarCB.autoInit = false;
	}

	void Start(){
		if (autoInit) {
			Init ();
		}
	}

	public void Init(){
		var allScalarCB = new List<ScalarControlBlockScript>{ 
			xScalarCB, yScalarCB, zScalarCB
		};

		if (playerPrefsKey == "") {
			Debug.LogError ("playerPrefsKey should present");

			allScalarCB.ForEach (x => x.numInput.SetErrorColor());
			titleLabel.text = "no playerPrefs key";
			return;
		}

		if (title == "") {
			if (playerPrefsKey != "") {
				title = playerPrefsKey;
			}
		}

		titleLabel.text = title;


		xScalarCB.title = "x";
		xScalarCB.defaultValue = defaultValue.x;
		xScalarCB.playerPrefsKey = playerPrefsKey+"_x";
		xScalarCB.oscAddress = oscAddress+"/x";

		yScalarCB.title = "y";
		yScalarCB.defaultValue = defaultValue.y;
		yScalarCB.playerPrefsKey = playerPrefsKey+"_y";
		yScalarCB.oscAddress = oscAddress+"/y";

		zScalarCB.title = "z";
		zScalarCB.defaultValue = defaultValue.z;
		zScalarCB.playerPrefsKey = playerPrefsKey+"_z";
		zScalarCB.oscAddress = oscAddress+"/z";



		// subscribe to change
		allScalarCB.ForEach(x => x.OnChange += OnScalarBlockChange);

		// now init
		allScalarCB.ForEach (x => x.Init ());
	}

	// EVENTS

	void OnScalarBlockChange(float val){
		value = new Vector3 (
			xScalarCB.value,
			yScalarCB.value,
			zScalarCB.value
		);

		OnChange (value);
	}
}
